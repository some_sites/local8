---
title: "quick start"
date: 2019-06-17T23:53:00+01:00
draft: false
hideLastModified: true
tags: ["tutorial", "quickstart", "documentation"]
showInMenu: true
---

# Installation

The following is required for installation / building:
 - git
 - make
 - go 1.16 +

The following is required for running:
 - helm 3+
 - helm diff plugin
 - kubectl
 - kustomize.io


{{< code language="bash" >}}
git clone https://gitlab.com/k8-experiments/local8
cd local8
make install
cd ../
{{< /code >}}


# Running


*please ensure that you are able to connect to a running k8 cluster*

 
## 0. Get some k8 resources

Grab the following file / folder if you don't have kustomize and helmfiles ready for deployment

{{< code language="bash" >}}
git clone https://gitlab.com/k8-experiments/resource-configs
cd resource-configs
{{< /code >}}

## 1. Initialize

This step finds helmfiles and kustomization files from the current dir and "indexes" them in `./l8/deploy.yaml`

{{< code language="bash">}}
local8 init
{{< /code >}}

## 2. Apply

Now we can deploy these by running

{{< code language="bash" >}}
local8 Apply
{{< /code >}}


# notes

In the example above we did not show how to modify what gets deployed.

